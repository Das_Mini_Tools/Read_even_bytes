﻿using System;
using System.IO;
using System.Windows.Forms;

namespace read_even_bytes
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 1 && args[0].ToLower() == "-version")
            {
                Console.WriteLine(String.Format("{0}, written by {1} for catalinnc", Application.ProductName, Application.CompanyName));
                
                Environment.Exit(0);
            }

            if (args.Length == 2)
            {
                string targetFile = args[1];

                byte[] sourceFile = File.ReadAllBytes(args[0]);

                byte[] targetBuffer;

                if (sourceFile.Length % 2 == 0)
                    targetBuffer = new byte[sourceFile.Length / 2];
                else
                    targetBuffer = new byte[(sourceFile.Length / 2) + 1];

                for (int i = 0; i < sourceFile.Length; i += 2) // Read even bytes only
                {
                    if (i > 0)
                        targetBuffer[i / 2] = sourceFile[i];
                    else
                        targetBuffer[0] = sourceFile[0];
                }

                using (FileStream stream = new FileStream(targetFile, FileMode.Create, FileAccess.Write, FileShare.None, 4096, FileOptions.SequentialScan))
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    writer.Write(targetBuffer);
                }

                Environment.Exit(0);
			}

            Console.WriteLine("Usage: sourcefile targetfile");
            Console.WriteLine("Example: eboot.elf \"halved eboot.elf\"");

            Environment.Exit(0);
        }
    }
}
